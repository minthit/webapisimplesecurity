﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiSimpleSecurityImp.Models
{
    public class PasswordModel
    {
        public string hashedPassword { get; set; }
        public string saltKey { get; set; }
    }
}