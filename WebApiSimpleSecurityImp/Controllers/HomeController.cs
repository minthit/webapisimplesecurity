﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApiSimpleSecurityImp.Models;
using WebApiSimpleSecurityImp.PasswordSecurity;

namespace WebApiSimpleSecurityImp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            string password = PasswordHasherHelper.Hash("abcd");
            ViewBag.HashedPassword = password;

            bool valid = PasswordHasherHelper.Verify("abcd", password);

            if(valid)
            {
                ViewBag.CheckPassword = "Valid";
            }
            else
            {
                ViewBag.CheckPassword = "Not Valid";
            }

            return View();
        }
    }
}
